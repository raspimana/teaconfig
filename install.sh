#!/bin/bash
# Cup o' tea?
game=none
i=0
#exec 3>&1

echo "Downloading TeaConfig..."
mkdir /tmp/teaconfig
cd /tmp/teaconfig || exit
curl https://codeberg.org/raspimana/teaconfig/archive/master.tar.gz --output master.tar.gz
tar -xvf master.tar.gz
cd teaconfig || exit
dir=$(pwd)
echo "Download succesful! Starting installer..."


bucket(){
    cd """$game/cfg""" || exit
    mkdir TEA_BACKUP
    for file in "${backup[@]}"; do
        mv "$file" TEA_BACKUP/
    done
    echo "Done backing up duplicate files..."
}
copy(){
    cd """$dir/cfg""" || exit
    cp -r ./* """$game/cfg"""

    dialog --title "TeaConfig" --yesno "Would you like to set custom comp. binds for Medic? \n(see README)" 7 35
    if [ $? -eq 0 ] ; then
        cp -i ./comp/medic.cfg """$game/cfg"""
    fi

    echo "Done installing!"
}

backup(){
    cd """$game""" || exit
    cp -r cfg cfg_BACKUP
    echo "Done backing up config folder..."
}

echo "Finding game directory..."
if [ -d /home/"$USER"/.steam/steam/steamapps/common/Team\ Fortress\ 2/tf ]; then
    game=/home/$USER/.steam/steam/steamapps/common/Team\ Fortress\ 2/tf
else
    echo "Default not found, asking user for location..."
    game=$(dialog --title "TeaConfig" --inputbox "TF2 directory not found? Where is it? (Example: /home/user/.steam/steam/steamapps/common/Team Fortress 2/tf)" 9 60 2>&1 1>&3)
fi

echo Looking for duplicate files...
cd """$game/cfg""" || exit
#tf2cfg=$(ls *.cfg)
#tf2IN=(${tf2cfg// / })
cd """$dir/cfg""" || exit
teacfg=$(ls ./*.cfg)
teaIN=("${teacfg// / }")
for file in "${teaIN[@]}"; do
    i+=1
    test -f """$game/cfg/$file""" && backup[i]=$file
done

if (: "${backup[@]?}") 2>/dev/null; then
    echo "Duplicate files found, backing up config folder and duplicates..."
    backup
    bucket
    echo "Installing..."
    copy
    dialog --title "TeaConfig" --msgbox "NOTE: Conflicting files have been found & put in '[game]/cfg/TEA_BACKUP'." 6 60
    dialog --title "TeaConfig" --msgbox "Done! If you ever need to reset your config, it's in '[game]/cfg_BACKUP'." 6 60
else
    echo "No duplicate files found, just backing up config folder..."
    backup
    echo "Installing..."
    copy
    dialog --title "TeaConfig" --msgbox "Done! If you ever need to reset your config, it's in '[game]/cfg_BACKUP'." 6 60
fi

